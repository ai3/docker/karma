docker-karma
===

Download and compile karma frontend for Alertmanager API.

https://github.com/prymitive/karma

The following environment variables can be used to configure karma:

* `ALERTMANAGER_URI` - default localhost:9093
* `LISTEN_ADDRESS` - default localhost:19194
