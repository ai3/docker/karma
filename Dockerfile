FROM registry.git.autistici.org/ai3/docker/s6-overlay-lite:master

ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get -q update && \
    apt-get install -qy --no-install-recommends karma && \
    apt-get clean && \
    rm -fr /var/lib/apt/lists/*

COPY conf /etc/


